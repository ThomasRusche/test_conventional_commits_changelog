# About #

Jenkins can generate changelogs automatically based on [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) with a [plugin](https://plugins.jenkins.io/git-changelog/).

Jenkins can also generate a new version number based on [semantic versioning](https://semver.org/lang/de/) with a [plugin](https://plugins.jenkins.io/conventional-commits/).

This is a demo project with appropriate git commit messages.



git tag -a v1.0.0 -m "Initial release"